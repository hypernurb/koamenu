/*! Koamenu - v0.1.0 - 2012-10-12
* Copyright (c) 2012 Kristijan Husak & Aleksandar Gosevski; Licensed MIT, GPL */

/*--------------------------------------
* MAIN JS FILE
---------------------------------------*/

(function($) {
	var namespace = 'slidemenu',
	menuOpen = false;

    function SlideMenu(element, options) {
        this.$element = $(element);
        var o = this.options = $.extend({}, $.fn.slideMenu.defaults, options);
        $(o.menu).css(o.position, 0).css('width', o.menuWidth);

        $(this.$element, '.dark-overlay').live('click.' + namespace, $.proxy(this.toggle, this));
    }

    SlideMenu.prototype = {
        $: function (selector) {
            return this.$element.find(selector);
        },

        toggle: function () {
            if (menuOpen) {
                this._closeMenu();
                menuOpen = false;
            } else {
                this._openMenu();
                menuOpen = true;
            }
        },

        _openMenu : function (e) {
            if (!menuOpen) {
                var o = this.options;
                var side = (o.position === 'left') ? o.menuWidth : '-' + o.menuWidth;
                $(o.mainContent).animate({'left': side}, o.speed);
                $(o.menu).fadeIn('fast');
                this._overlay();
                menuOpen = true;
            }
        },

        _closeMenu : function (e) {
            if (menuOpen) {
                var o = this.options;
                $(o.mainContent).animate({'left': 0}, o.speed);
                $(o.menu).fadeOut('fast');
                this._overlay('remove');
                menuOpen = false;
            }
        },

        _overlay : function (action) {
            var o = this.options;
            if (action === 'remove') {
                $('.dark-overlay').remove();
            } else {
                $(o.mainContent).prepend('<div class="dark-overlay"></div>');
                $('.dark-overlay').css({
                    width : $(o.mainContent).width(),
                    height : $(o.mainContent).height(),
                    position : 'absolute',
                    backgroundColor : 'rgba(0,0,0,0.4)',
                    top : 0,
                    left : 0
                });
            }
        },

        _resize : function () {
            if ($('.dark-overlay').length && $('.dark-overlay') !== undefined) {
                $('.dark-overlay').css({
                    'width' : $(this.options.mainContent).width(),
                    'height': $(this.options.mainContent).height()
                });
            }
        }

    };

    $.fn.slideMenu = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new SlideMenu(this, options)));
            }

            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.slideMenu.Constructor = SlideMenu;

    $.fn.slideMenu.defaults = {
        position : 'left',
        menu : '.menu',
        mainContent : '.main-content',
        speed : 500,
        menuWidth : '50%'
    };

}(jQuery));