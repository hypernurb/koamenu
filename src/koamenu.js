/*
 * Koamenu
 *
 *
 * Copyright (c) 2012 Kristijan Husak & Aleksandar Gosevski
 * Updated by: Ivan Tatic ( ivan@simplified.me )
 * Licensed under the MIT, GPL licenses.
 */

(function($) {
	var namespace = 'slidemenu',
	menuOpen = false;

    function SlideMenu(element, options) {
        var o = this.options = $.extend({}, $.fn.slideMenu.defaults, options);

        this.$element = $(element);
        this.$overlay = $('<div>', { 'class': 'overlay' });
        this.$menu = $(o.menu);
        this.$content = $(o.mainContent).css({ position: 'relative'});
        this.isOpen = false;

        this.$content.append( this.$overlay.css( o.overlay ).hide() );
        this.$menu.css(o.direction, 0).css('width', o.menuWidth);

        this.$element.on('click.' + namespace, $.proxy(this.toggle, this));
        this.$overlay.on('click.' + namespace, $.proxy(this.toggle, this));
    }

    SlideMenu.prototype = {
        $: function (selector) {
            return this.$element.find(selector);
        },
        toggle: function(e) {
            e && e.preventDefault();

            this._toggleState();
        },
        _toggleState: function() {
            var o = this.options,
                animation = {};

            animation[ o.direction ] = this.isOpen ? '0%' : o.menuWidth;
console.log( o, animation );
            this.$content.animate( animation, o.speed );
            this.$menu.fadeToggle( o.speed );
            this.$overlay.toggle();

            this.isOpen = !this.isOpen;
        }
    };

    $.fn.slideMenu = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new SlideMenu(this, options)));
            }

            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.slideMenu.Constructor = SlideMenu;

    $.fn.slideMenu.defaults = {
        direction : 'left',
        menu : '.menu',
        mainContent : '.main-content',
        speed : 500,
        menuWidth : '50%',
        overlay: {
            left: 0,
            right: 0,
            bottom: 0,
            top: 0,
            position: 'absolute',
            background: 'rgba(0,0,0,.4)',
            userSelect: 'none',
            zIndex: '99'
        }
    };

}(jQuery));
