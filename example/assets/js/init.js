$(function () {
	$('.btn-left').slideMenu({
		menuWidth: '70%',
		'menu': '.menu-left'
	});

	$('.btn-right').slideMenu({
		menuWidth: '70%',
		direction: 'right',
		menu: '.menu-right'
	});

});
